@title Describing tree-based grammars with XMG

@s Introduction

[XMG](https://github.com/spetitjean/XMG-2/wiki#what-is-xmg-2-) is a description language which has been designed to provide linguists with a tool to describe in an expressive and yet abstract way redundant tree-based formal grammars. Grammar descriptions in XMG (that is grammar source codes, aka "meta-grammars") are compiled by a specific piece of software, namely the  [XMG compiler](https://sourcesup.renater.fr/xmg) and since 2010 the [XMG2 compiler](https://github.com/spetitjean/XMG-2/wiki/1:--First-steps#installation).

@s A toy meta-grammar in XMG

A metagrammar written in XMG is composed of (in this order):
 - a header (containing type and feature declarations)
 - tree fragments (described using tree description logic formulas, and encapsulated within **classes**)
 - fragment combinations (via conjunction or disjunction of classes)
 - valuations (list of classes referring to *complete* descriptions and which need to be compiled)

Let us have a look at what a header looks like.

First, we can use some pre-defined well-formedness principles (not mandatory). One instance of such a principle is the **unicity** principle which can be invoked to ensure a given *(feature,value)* pair cannot appear more than once in an output grammar rule.

--- Principles
use unicity with (gf = subj) dims (syn)
use unicity with (gf = obj) dims (syn)
---

It is also possible to define macros (strings to be replaced on-the-fly during compilation):

--- Macro
macro realcat s
---

Then, the grammar designer has to define the types that will be used within the metagrammar:

--- Types
type MARK = {subst, subst, nadj, foot, anchor, coanchor, flex }
type NAME = {SubjNode, ObjNode, Anchor}
type CAT = {n,v,s}
type GF = {subj, obj}
type lexicon = {manger}
---

and also the properties (which can be used to define nodes' specific pieces of information, used during tree description solving):

--- Properties
property mark : MARK
property name : NAME
property gf : GF
---

Typed features can be defined as well:

--- Features
feature cat : CAT
---

Now the main part: definition of tree fragments (co-called **classes**)

Here, we define an **Active** fragment, consisting of two nodes (associated with variables ?X and ?Y and such that ?X dominates ?Y, these variables are exported so that they can be accessed from outside this class):


--- Active
class Active
export ?X ?Y
declare ?X ?Y
{<syn>{
        ?X -> ?Y
        }
}
---

Next, we define another tree fragment which uses three nodes with specific properties (inside parentheses) and features (in square brackets):


--- CanSubject
class CanSubject
export ?X ?Y ?Z
declare ?X ?Y ?Z
{ <syn>{
        node ?X [cat = realcat]{
                node ?Z (name=SubjNode, mark = subst, gf=subj)[cat=n]
                node ?Y [cat = v]
                }
        }
}
---

The **Object** class is used to describe a canonical nominal object:

--- Object
class Object
export ?X ?Y ?Z
declare ?X ?Y ?Z
{ <syn>{
        node ?X [cat = s]{
                node ?Y (name=Anchor)[cat = v]
                node ?Z (name=ObjNode, mark=subst,gf=obj)[cat=n]
                }
        }
}
---

Let us say a subject can also be realised as a relative subject, we need an extra fragment for this:

--- RelSubject
class RelSubject
export ?X ?Y ?Z
declare ?X ?Y ?Z ?U ?V
{ <syn>{
        node ?U [cat = n]{
                node ?V [cat = n]
                node ?X [cat = s]{
                        node ?Z (name=SubjNode, mark=subst,gf=subj)[cat=n]
                        node ?Y [cat = v]
                        }
                }
        }
}
---

We define an abstraction over all possible realisations of a subject (canonical and relative here):

--- Subject
class Subject
export ?X ?Y
declare ?X ?Y ?Can ?Rel
{
  {?Can = CanSubject[] ; ?X=?Can.?X ; ?Y=?Can.?Y }
  |
  {?Rel = RelSubject[] ; ?X=?Rel.?X ; ?Y=?Rel.?Y }
}
---

Let us now define what a transitive verb is.
To do so, *Subject* is conjuncted with a verb and an object:

--- transitive
class transitive
declare ?Subj ?Verb ?Obj
{
    ?Subj=Subject[] ; ?Verb=Active[] ; ?Obj = Object[]
	; ?Subj.?X = ?Verb.?X ; ?Verb.?X = ?Obj.?X
	; ?Subj.?Y = ?Verb.?Y ; ?Verb.?Y = ?Obj.?Y
}
---

Finally let us declare the classes which refer to complete description (only transitive here):

--- value
value transitive
---

All together, it gives us the following toy XMG metagrammar:

--- toy.xmg
@{Principles}
@{Macro}
@{Types}
@{Properties}
@{Features}

@{Active}
@{CanSubject}
@{Object}
@{RelSubject}
@{Subject}
@{transitive}

@{value}
---

You can compile this metagrammar (file toy.xmg) by uploading it to [http://xmg.phil.hhu.de](http://xmg.phil.hhu.de) (workbench section), and select the **synsem** compiler in the graphical user interface.
